#include "74HC165.h"

void HC165_Class::Init(void)
{
	CP.Init(GPIO_Mode_OUT);
	PL.Init(GPIO_Mode_OUT);
	Q7.Init(GPIO_Mode_IN);

	CP.Clear();	//清0
	PL.Set();	//置1
}

uint16_t HC165_Class::Read(uint8_t bit_num)
{
	uint16_t temp = 0;
	PL.Clear();
	PL.Set();	//上升沿锁存

	for (int i = 0; i < bit_num; i++)
	{
		temp <<= 1;
		temp |= Q7.Read();
		//HC165_Value.Value <<= 1;
		//HC165_Value.Value |= Q7.Read();
		CP.Set();
		CP.Clear();
	}
	//return HC165_Value.Value;
	return temp;
}
