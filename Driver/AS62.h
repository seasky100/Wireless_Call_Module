#pragma once
#include "Wireless_Serial.h"

class AS62_Class final :public WirelessSerial_Class
{
public:
	AS62_Class() :WirelessSerial_Class() {}
	~AS62_Class() = default;

	uint32_t Return_Baudrate(void) override;	//返回串口波特率
	void Mode(WF_Mode mode, bool value) override;

private:

	bool Analyze_Para_Data(const char*rx) override;	//分析参数数据
	void Code_Para_Data(void) override;	//对参数进行编码,发送
	void Send_Read_CMD(void) override;	//发送读参数指令
};
