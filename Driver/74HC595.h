#pragma once
#include "../HALayer/IO.h"

class HC595_Class
{
public:
	HC595_Class() :RCLK(GPIOA, GPIO_Pin_6), SRCLK(GPIOA, GPIO_Pin_7), SER(GPIOA, GPIO_Pin_5), OE(GPIOB, GPIO_Pin_1) {}
	~HC595_Class() = default;

	void Init(void);	//初始化74HC595
	inline void Write(void) { Write(HC595_Value.value); }
	void Write(const uint8_t data);	//写入一个字节，bit7对应Q7，bit0对应Q0

	union
	{
		volatile const uint8_t value = 0;
		struct
		{
			unsigned Power_LED : 1;	//QA
			unsigned Call_R : 1;	//QB
			unsigned Call_B : 1;	//QC
			unsigned Back_R : 1;	//QD
			unsigned Back_B : 1;	//QE
			unsigned Uart_Set0 : 1;	//QF
			unsigned Uart_Set1 : 1;	//QG
			unsigned Buzzer : 1;		//QH
		};//先分配低位

	}HC595_Value;

private:
	IO RCLK;
	IO SRCLK;
	IO SER;
	IO OE;

	//uint8_t const *const value;

	void Refresh(void);	//将移位寄存器内的数据输出到端口
};
