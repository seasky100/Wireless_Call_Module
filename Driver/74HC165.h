#pragma once
#include "../HALayer/IO.h"


//在本应用中，读出顺序为返回按钮，呼叫按钮，2Pin拨码1-2、4Pin拨码1-4，8Pin拨码1-8
//在本应用中，读出的值全为反相，即按键按下检测到为1，拨码开关拨到ON为1

/*
并入串出逻辑芯片74HC165
CP	串行移位时钟	上升沿采样
PL	输入锁存时钟	低电平锁存
Q7	串行数据输出	D7先出
*/

class HC165_Class
{
public:

	HC165_Class() :CP(GPIOA, GPIO_Pin_3), PL(GPIOA, GPIO_Pin_4), Q7(GPIOA, GPIO_Pin_2) {};
	~HC165_Class() = default;


	void Init(void);	//初始化74HC165
	uint16_t Read(uint8_t bit_num);	//使用74HC165读取指定数量的位，先读取高位

private:
	
	IO CP;	//串行移位时钟
	IO PL;	//锁存端口信号
	IO Q7;	//串行数据输出
};
