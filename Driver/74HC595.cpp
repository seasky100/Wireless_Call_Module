#include "74HC595.h"

void HC595_Class::Init(void)
{
	SRCLK.Init(GPIO_Mode_OUT);
	RCLK.Init(GPIO_Mode_OUT);
	SER.Init(GPIO_Mode_OUT);
	OE.Init(GPIO_Mode_OUT, GPIO_OType_OD);

	RCLK.Clear();
	SRCLK.Clear();
	SER.Clear();
	OE.Set();	//开漏输出，上拉至高电平

	//*(uint8_t *)(&HC595_Value) = 0;
	HC595_Value.Power_LED = 1;

	//Write(HC595_Value.value);
	Write();
	Refresh();

	OE.Clear();	//使能74HC595输出
}

void HC595_Class::Write(const uint8_t data)
{
	for (int8_t i = 7; i >= 0; i--)
	{
		if ((data >> i) & 0x01)
		{
			SER.Set();
		}
		else
		{
			SER.Clear();
		}
		SRCLK.Set();
		SRCLK.Clear();
	}
	Refresh();
}

void HC595_Class::Refresh(void)
{
	RCLK.Set();
	RCLK.Clear();
}
