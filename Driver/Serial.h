#pragma once
#include "../HALayer/Uart.h"
#include "../HALayer/Tim.h"
#include <stm32f0xx_misc.h>


/*
* 该串口（串口1）用于和AGV、物料中心通信
* 查询方式发送数据，定时器14超时检测接收数据帧
*/

extern "C" void TIM14_IRQHandler(void);	//用于检测数据帧
extern "C" void USART1_IRQHandler(void);

class Serial_Class :public Uart_Base_Class
{
	friend void TIM14_IRQHandler(void);	//使用定时器做串口接收超时检测，需进行友元申明
	friend void USART1_IRQHandler(void);	//串口中断函数，友元申明

public:
	Serial_Class() :Uart_Base_Class(USART1) {}
	virtual ~Serial_Class() = default;

	void Init(uint32_t baudrate = 9600);	//根据波特率初始化串口

	bool Return_rx_flag(void) { return rx_flag; }
	void Clear_rx_flag(void) { rx_flag = false; }
	uint16_t Return_rx_cnt(void) { return rx_cnt; }
	void Clear_rx_cnt(void) { rx_cnt = 0; }
	char* Return_RX_buf(void) { return (char*)RX_buf; }

private:
	static volatile char RX_buf[256];	//接收数据的缓冲区

	static uint16_t rx_cnt;	//接收字节的计数
	static bool rx_flag;	//接收数据是否完成

	void write(const char c) override;
	static void write_RX(const char c);
};


