#pragma once
#include <stm32f0xx_gpio.h>

class IO
{
public:
	IO() = default;
	IO(GPIO_TypeDef * GPIO, uint16_t Pin) :GPIO(GPIO), Pin(Pin) {};
	~IO() = default;
	void Init(GPIO_InitTypeDef *GPIO_InitStructure);
	void Init(GPIOMode_TypeDef GPIO_Mode, GPIOOType_TypeDef GPIO_OType = GPIO_OType_PP, GPIOPuPd_TypeDef GPIO_PuPd = GPIO_PuPd_NOPULL, GPIOSpeed_TypeDef GPIO_Speed = GPIO_Speed_10MHz);
	inline void Set(void) { GPIO->BSRR = Pin; }
	inline void Clear(void) { GPIO->BRR = Pin; }
	inline bool Read(void) { return (GPIO->IDR)&Pin; }
	inline void Toggle(void) { GPIO->ODR ^= Pin; }
	void Write(bool value);
private:
	GPIO_TypeDef * GPIO;
	uint16_t Pin;
};



