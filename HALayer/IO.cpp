#include "IO.h"

void IO::Init(GPIO_InitTypeDef * GPIO_InitStructure)
{
	GPIO_InitStructure->GPIO_Pin = Pin;
	GPIO_Init(GPIO, GPIO_InitStructure);
}

void IO::Init(GPIOMode_TypeDef GPIO_Mode, GPIOOType_TypeDef GPIO_OType, GPIOPuPd_TypeDef GPIO_PuPd, GPIOSpeed_TypeDef GPIO_Speed)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.GPIO_Mode = GPIO_Mode;
	GPIO_InitStructure.GPIO_OType = GPIO_OType;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed;
	//GPIO_InitStructure.GPIO_Pin =Pin;
	Init(&GPIO_InitStructure);
}

void IO::Write(bool value)
{
	value ? Set(): Clear();
}
