#include "Key.h"

void KEY::Scan(bool value)
{
	switch (key_state)
	{
	case NO_KEY:
		if (value)
		{
			key_state = CHECK_KEY;
		}
		break;
	case CHECK_KEY:
		key_state = value ? ENSURE_KEY : NO_KEY;
		//if (value)
		//{
		//	key_state = ENSURE_KEY;
		//}
		//else
		//{
		//	key_state = NO_KEY;
		//}
		break;
	case ENSURE_KEY:
		if (!value)
			key_state = RELEASE_KEY; //检测到按键松开
		break;
	case RELEASE_KEY:
		if (value)
			key_state = ENSURE_KEY;
		else
		{
			pb = true;
			key_state =NO_KEY; //按键未按下
		}
		break;
	default:
		break;
	}
}
