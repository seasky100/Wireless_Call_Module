#pragma once
#include <stm32f0xx_usart.h>
#include <stm32f0xx_gpio.h>
#include <string>

/*
* Uart类负责写入
*	一个C风格的字符串
*	一个指定长度的数组
*	//一个string类对象(该应用中不需要使用string类)
*	一个字符
*	//格式化整形、浮点型(该应用中不需要格式化整形、浮点型)
*
* 具体的写入方法write为抽象函数，需子类自己定义
*
* 若要使用串口，必须先执行enable()，打开串口
*/

class Uart_Base_Class
{
public:
	Uart_Base_Class(USART_TypeDef * UartX) :Uart(UartX) {};
	virtual ~Uart_Base_Class() = default;

	inline void print(const char *str) { while (*str) write(*str++); }	//c语言风格字符串
	inline void print(const unsigned char* buffer, unsigned int size) { print((char*)buffer, size); }	//指定长度无符号型数组
	inline void print(const char* buffer, unsigned int size) { while (size--) write(*buffer++); }	//指定长度数组
	//inline void print(const std::string& s) { for (auto i : s) { write(i); } }

	inline void print(unsigned char b) { print((char)b); }
	inline void print(char c) { write(c); }

	//inline void print(char c, int base = 0) { print((long)c, base); }
	//inline void print(unsigned char b, int base = 0) { print((unsigned long)b, base); }
	//inline void print(int n, int base = 10) { print((long)n, base); }
	//inline void print(unsigned int n, int base = 10) { print((unsigned long)n, base); }
	//void print(long n, int base = 10);
	//void print(unsigned long n, int base = 10);
	//inline void print(float n, int digits = 2) { printFloat(n, digits); }

protected:
	void Init(USART_InitTypeDef * Usart_InitStructure) { USART_Init(Uart, Usart_InitStructure); };	//初始化串口
	inline void enable(void) { Uart->CR1 |= (USART_CR1_UE); }	//开启串口
	inline void disable(void) { Uart->CR1 &= ~(USART_CR1_UE); }	//关闭串口

	USART_TypeDef * Uart;

private:
	virtual void write(const char c) = 0;	//写一个字符
	//void printNumber(unsigned long n, const unsigned char base);
	//void printFloat(float number, unsigned char digits);

};
