#pragma once

class LED
{
public:
	LED() = default;
	~LED() = default;

	int cnt;	//亮灯时间计数
	bool flag;	//亮灯指示，1表示亮，0表示不亮
	bool Check(void);	//检查灯是否需要点亮

private:

};