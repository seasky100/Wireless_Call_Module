#include "Tim.h"

void TIM_Base_Class::Init(uint16_t arr, uint16_t psc)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

	//TIM_DeInit(TIM);
	TIM_TimeBaseStructure.TIM_Period = arr - 1;
	TIM_TimeBaseStructure.TIM_Prescaler = psc - 1;
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

	TIM_TimeBaseInit(_TIMx, &TIM_TimeBaseStructure);
	_TIMx->CR1 &= ~(1 << 1);	//清空UDIS,使能更新中断UEV
	_TIMx->CR1 |= (1 << 2);	//置位URS,只有计数器上溢会生成UEV（更新中断）
	_TIMx->SR = ~TIM_IT_Update;	//清零更新中断标志
}

void TIM_Base_Class::Init_Interrupt(uint16_t arr, uint16_t psc)
{
	Init(arr, psc);
	_TIMx->DIER |= TIM_IT_Update;	//使能更新中断
}

void Init_Time(TIM_TypeDef * TIM, uint16_t arr, uint16_t psc)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

	TIM_DeInit(TIM);

	TIM_TimeBaseStructure.TIM_Period = arr - 1;
	TIM_TimeBaseStructure.TIM_Prescaler = psc - 1;
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

	TIM_TimeBaseInit(TIM, &TIM_TimeBaseStructure);
	TIM->SR = ~TIM_IT_Update;
	TIM_ITConfig(TIM, TIM_IT_Update, ENABLE);	//使能更新中断
}
