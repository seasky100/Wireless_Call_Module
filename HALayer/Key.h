#pragma once

enum KEY_STATE
{
	NO_KEY,	//未检测到按键
	CHECK_KEY,	//检测到按键按下
	ENSURE_KEY,	//确定按键按下
	RELEASE_KEY	//按键松开
};

class KEY
{
public:
	KEY() = default;
	virtual ~KEY() = default;

	bool pb;	//pb=true表示按键按下，pb=false表示按键未按下
	enum KEY_STATE key_state;

	void Scan(bool value);	//按键扫描，当按键松开时，pb置1
private:

};
