#pragma once
#include <stm32f0xx_tim.h>

class TIM_Base_Class
{
public:
	TIM_Base_Class(TIM_TypeDef * TIMx) :_TIMx(TIMx) {}
	virtual ~TIM_Base_Class() = default;

	void Init(uint16_t arr, uint16_t psc);	//初始化定时器，打开中断
	void Init_Interrupt(uint16_t arr, uint16_t psc);	//初始化定时器，不打开中断
	void Begin(void) { _TIMx->CR1 |= TIM_CR1_CEN; }	//开启定时器
	void Stop(void) { _TIMx->CR1 &= ~TIM_CR1_CEN; }	//关闭定时器

private:

protected:
	TIM_TypeDef * _TIMx;	//使用的定时器
};

void Init_Time(TIM_TypeDef * TIM, uint16_t arr, uint16_t psc);
inline void Time_Open(TIM_TypeDef * TIM) { TIM->CR1 |= TIM_CR1_CEN; }//开启定时器
inline void Time_Close(TIM_TypeDef * TIM) { TIM->CR1 &= ~TIM_CR1_CEN; }//关闭定时器s